package developer.ink1804.bottomsheettest

import android.graphics.drawable.Drawable

data class ApplicationModel(
        var name: String,
        var icon: Drawable
)