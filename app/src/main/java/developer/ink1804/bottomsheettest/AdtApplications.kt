package developer.ink1804.bottomsheettest

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.adt_item.view.*

class AdtApplications(var items: List<ApplicationModel>) : RecyclerView.Adapter<AdtApplications.VhApplication>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VhApplication {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.adt_item, parent, false)
        return VhApplication(view)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: VhApplication, position: Int) {
        holder.bind(items[position])
    }


    inner class VhApplication(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(model: ApplicationModel) {
            itemView.ivIcon.setImageDrawable(model.icon)
            itemView.tvName.text = model.name
        }
    }
}