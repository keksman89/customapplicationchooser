package developer.ink1804.bottomsheettest

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.frg_first.view.*

class FirstFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        var root = inflater.inflate(R.layout.frg_first, container, false)

        root.btNext.setOnClickListener {
            parentFragment!!.childFragmentManager
                    .beginTransaction()
                    .replace(R.id.fragmentContainer, SecondFragment())
                    .addToBackStack("second")
                    .commit()
        }

        root.btClose.setOnClickListener {
            (parentFragment!! as BottomSheetFragment).dismiss()
        }

        return root
    }
}