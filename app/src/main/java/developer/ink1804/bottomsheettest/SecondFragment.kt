package developer.ink1804.bottomsheettest

import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.GridLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.frg_second.view.*

class SecondFragment : Fragment() {


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val root = inflater.inflate(R.layout.frg_second, container, false)

        root.btPrevious.setOnClickListener {
            parentFragment!!.childFragmentManager.popBackStack()
        }

        val pm = context!!.packageManager
        val intent = Intent(Intent.ACTION_VIEW, Uri.parse("geo:0,0?q="))
        val getAppsList = pm.queryIntentActivities(intent,
                PackageManager.GET_META_DATA)

        var modelList = mutableListOf<ApplicationModel>()

        for (i in getAppsList) {
            val model = ApplicationModel(
                    pm.getApplicationLabel(i.activityInfo.applicationInfo).toString(),
                    pm.getApplicationIcon(i.activityInfo.packageName))
            modelList.add(model)
        }

        root.recycler.layoutManager = GridLayoutManager(context, 3)
        root.recycler.adapter = AdtApplications(modelList)

        return root
    }

}