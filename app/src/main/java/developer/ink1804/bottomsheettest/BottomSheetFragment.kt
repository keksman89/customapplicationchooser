package developer.ink1804.bottomsheettest

import android.os.Bundle
import android.support.design.widget.BottomSheetDialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

class BottomSheetFragment : BottomSheetDialogFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        var root = inflater.inflate(R.layout.frg_bottom_sheet, container, false)

        childFragmentManager
                .beginTransaction()
                .add(R.id.fragmentContainer, FirstFragment())
                .addToBackStack("first")
                .commit()

        return root
    }
}